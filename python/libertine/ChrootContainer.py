# Copyright 2015-2017 Canonical Ltd.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import psutil
import re
import shlex
import shutil
import subprocess
import tarfile

import requests

from .Libertine import BaseContainer
from . import utils
from libertine.ContainersConfig import ContainersConfig


def chown_recursive_dirs(path):
    uid = 0
    gid = 0

    if 'SUDO_UID' in os.environ:
        uid = int(os.environ['SUDO_UID'])
    if 'SUDO_GID' in os.environ:
        gid = int(os.environ['SUDO_GID'])

    if uid != 0 and gid != 0:
        for root, dirs, files in os.walk(path):
            for d in dirs:
                os.chown(os.path.join(root, d), uid, gid)
            for f in files:
                os.chown(os.path.join(root, f), uid, gid)

        os.chown(path, uid, gid)


class LibertineChroot(BaseContainer):
    """
    A concrete container type implemented using bubblewrap sandboxing
    tool - functionally similar to a plain old chroot running unprivileged.
    """

    def __init__(self, container_id, config, service):
        super().__init__(container_id, 'chroot', config, service)
        os.environ['DEBIAN_FRONTEND'] = 'noninteractive'
        os.environ['FAKEROOTDONTTRYCHOWN'] = '1'
        os.environ['SYSTEMD_OFFLINE'] = '1'

        # Use fakeroot to run apt and dpkg commands
        self.apt_command_prefix = 'fakeroot ' + self.apt_command_prefix
        self.dpkg_command_prefix = 'fakeroot ' + self.dpkg_command_prefix

    def run_in_container(self, command_string):
        cmd_args = shlex.split(command_string)
        command_prefix = self._build_bwrap_command()

        args = shlex.split(command_prefix + ' ' + command_string)
        cmd = subprocess.Popen(args)
        return cmd.wait()

    def destroy_libertine_container(self, force=True):
        return self._delete_rootfs()

    # We cannot simply substitute Ubuntu version into the URL, as the filename
    # includes point release, something we don't easily have access to. So pull
    # down SHA256SUMS and find out the latest tarball for the architecture.
    def _get_ubuntu_base_tarball_url(self):
        base_url = f"https://cdimage.ubuntu.com/ubuntu-base" \
                   f"/releases/{self.installed_release}/release"
        sha256sum_content = requests.get(f"{base_url}/SHA256SUMS").text

        re_split = re.compile(r' [ *]') # '  ' or ' *'
        tarball_filename = None
        for line in sha256sum_content.splitlines():
            _, filename = re_split.split(line)
            if filename.endswith(f'-{self.architecture}.tar.gz'):
                tarball_filename = filename
                # Keep the last file matched, as that's the newest.

        if not tarball_filename:
            raise RuntimeError(utils._('Unable to find correct Ubuntu Base tarball'))

        return f"{base_url}/{tarball_filename}"

    def _bwrap_path(self):
        bwrap_path = shutil.which('bwrap')
        if not bwrap_path:
            raise RuntimeError(utils._('executable bwrap not found'))

        return bwrap_path

    def create_libertine_container(self, password=None, multiarch=False):
        # Save and temporary unset LD_PRELOAD to avoid missing libtls-padding.so spam
        ld_preload = os.environ.get('LD_PRELOAD', '')
        if 'libtls-padding.so' in ld_preload:
            del os.environ['LD_PRELOAD']

        # Use Ubuntu Base as the starting point of our chroot.
        tarball_url = self._get_ubuntu_base_tarball_url()
        utils.get_logger().info(utils._("Downloading {tarball_url}").format(
            tarball_url=tarball_url))

        tarball_res = requests.get(tarball_url, stream=True)
        tarball_tar = tarfile.open(fileobj=tarball_res.raw, mode='r|*')

        os.makedirs(self.root_path, exist_ok=True)
        tarball_tar.extractall(path=self.root_path)

        # Install fakeroot inside the container.
        # This is the only time we use bwrap to fake root permission.
        # Because we didn't do newuidmap dance, setuid() and friends will fail.
        # Thus, tell Apt not to bother.
        commands = [
            f"{self._bwrap_path()}" \
                f" --bind {self.root_path} /" \
                f" --proc /proc --dev /dev" \
                f" --ro-bind /etc/resolv.conf /etc/resolv.conf" \
                f" --uid 0 --gid 0 --" \
                f" apt-get -o APT::Sandbox::User=root update",
            f"{self._bwrap_path()}" \
                f" --bind {self.root_path} /" \
                f" --proc /proc --dev /dev" \
                f" --ro-bind /etc/resolv.conf /etc/resolv.conf" \
                f" --uid 0 --gid 0 --" \
                f" apt-get -o APT::Sandbox::User=root install -y fakeroot",
        ]

        for command_line in commands:
            args = shlex.split(command_line)
            cmd = subprocess.Popen(args)
            cmd.wait()

            if cmd.returncode != 0:
                utils.get_logger().error(utils._("Failed to create container"))
                self.destroy_libertine_container()
                return False

        # Make sure user-data dir exists before calling run_in_container
        self._create_libertine_user_data_dir()

        self.run_in_container(f"{self.apt_command_prefix} install -y locales")
        self.update_locale()

        if multiarch and self.architecture == 'amd64':
            utils.get_logger().info(utils._("Adding i386 multiarch support..."))
            self.run_in_container(self.dpkg_command_prefix + " --add-architecture i386")

        # Copy third-party Apt sources setup from the host.
        for dir in [
            "/etc/apt/sources.list.d",
            "/etc/apt/trusted.gpg.d",
            "/etc/apt/preferences.d",
        ]:
            os.makedirs(f"{self.root_path}/{dir}", exist_ok=True)

            try:
                for file in os.listdir(dir):
                    if file.startswith('ubuntu'):
                        # We want to leave those alone.
                        continue

                    shutil.copy(
                        f"{dir}/{file}", f"{self.root_path}/{dir}/{file}")
            except FileNotFoundError:
                pass

        # "Unminimize" unpacked Ubuntu Base rootfs. `unminimize` is designed
        # to be interactive, thus using `yes` command to simulate that.
        self.run_in_container(f"{self.apt_command_prefix} install -y unminimize")
        self.run_in_container("sh -c 'yes | fakeroot unminimize'")

        utils.get_logger().info(utils._("Updating the contents of the container after creation..."))
        self.update_packages()

        for package in self.default_packages:
            if not self.install_package(package, update_cache=False):
                utils.get_logger().error(utils._("Failure installing '{package_name}' during container creation".format(package_name=package)))
                self.destroy_libertine_container()
                return False

        # Install libtls-padding.so and restore LD_PRELOAD if needed
        if 'libtls-padding.so' in ld_preload:
            self.run_in_container('fakeroot apt-get install -y libtls-padding0')
            os.environ['LD_PRELOAD'] = ld_preload

        # Check if the container was created as root and chown the user directories as necessary
        chown_recursive_dirs(utils.get_libertine_container_home_dir(self.container_id))

        super().create_libertine_container()

        return True

    def _build_bwrap_command(self):
        bwrap_cmd = self._bwrap_path()

        bwrap_cmd += ' --bind "{}" /'.format(self.root_path)
        bwrap_cmd += ' --dev-bind /dev /dev --proc /proc --ro-bind /sys /sys'
        bwrap_cmd += ' --tmpfs /run --bind /tmp /tmp'
        bwrap_cmd += ' --ro-bind /etc/resolv.conf /etc/resolv.conf'

        # Bind-mount extrausers on the phone
        if os.path.exists("/var/lib/extrausers"):
            bwrap_cmd += " --ro-bind /var/lib/extrausers /var/lib/extrausers"

        home_path = os.environ['HOME']

        # Use a separate user home, but bind-mount common XDG directories inside
        bind_mounts = ' --bind "{}" "{}"'.format(
            utils.get_libertine_container_home_dir(self.container_id), home_path)

        mounts = self._sanitize_bind_mounts(utils.get_common_xdg_user_directories() + \
                                            ContainersConfig().get_container_bind_mounts(self.container_id))
        for user_dir in utils.generate_binding_directories(mounts, home_path):
            if os.path.isabs(user_dir[1]):
                path = user_dir[1]
            else:
                path = os.path.join(home_path, user_dir[1])

            bind_mounts += ' --bind "{}" "{}"'.format(user_dir[0], path)

        bwrap_cmd += bind_mounts

        # Bind-mount XDG_RUNTIME_DIR for mirclient/Wayland connection
        xdg_runtime_dir = os.environ['XDG_RUNTIME_DIR']
        bwrap_cmd += ' --bind "{0}" "{0}"'.format(xdg_runtime_dir)

        user_dconf_path = os.path.join(home_path, '.config', 'dconf')
        if os.path.exists(user_dconf_path):
            bwrap_cmd += " --bind {0} {0}".format(user_dconf_path)

        return bwrap_cmd

    def _sanitize_bind_mounts(self, mounts):
        return [mount.replace(" ", "\\ ").replace("'", "\\'").replace('"', '\\"') for mount in mounts]

    def start_application(self, app_exec_line, environ):
        # Workaround issue where a custom dconf profile is on the machine
        if 'DCONF_PROFILE' in environ:
            del environ['DCONF_PROFILE']

        bwrap_cmd = self._build_bwrap_command()

        args = shlex.split(bwrap_cmd)
        args.extend(app_exec_line)
        return psutil.Popen(args, env=environ)

    def finish_application(self, app):
        app.wait()
